/*jslint unparam: true, regexp: true */
/*global window, $ */
$(function () {
    'use strict';
    $('#fileupload').fileupload({
        url: "upload/do_upload",
        dataType: 'json',
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|mp4|wmv|mov|avi|tif|bmp)$/i,
        maxFileSize: 99900000,
        filesContainer:$(".files"),
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        // previewMaxWidth: 100,
        // previewMaxHeight: 100,
        // previewCrop: true
        messages: {
                acceptFileTypes: 'Dieses Dateiformat wird nicht unterstützt, bitte wählen Sie eines der folgenden Formate aus: .jpg, .png, .tif, .bmp, .mp4, .mov, .wmv, .avi, *.pdf',
            }
    }).on('fileuploaddone', function (e, data) {
        // $.each(data.result.files, function (index, file) {
        //     if (file.url) {
        //        //  $('<input>',{
        //        //     name:"image[]",
        //        //     type:"hidden",
        //        //     value:file.pdfUrl
        //        // }).appendTo('#fileupload');
        //     } 
        // });
    }).prop('disabled', !$.support.fileInput)
    .parent().addClass($.support.fileInput ? undefined : 'disabled');
});