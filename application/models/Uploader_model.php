<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uploader_model extends CI_Model {
	public function save($object) {
		$this->db->insert('form_data', $object);
		return $this->db->insert_id();
	}
	public function save_image($object) {
		$this->db->insert('uploaded_files', $object);
		return $this->db->insert_id();
	}
	function get_all($seach_option) {
		$term = ($seach_option['term'] == "aends") ? NULL : $seach_option['term'];
		$this->db->select();
		$this->db->from('form_data');
		$this->db->join('uploaded_files pf', 'pf.parent_id = form_data.id');
		if ($term) {
			$this->db->like('form_data.username', $term);
			$this->db->or_like('form_data.email', $term);
		}
		$this->db->order_by('id', 'desc');
		return $this->db->get()->result_array();
	}
	function get($id) {
		$this->db->select();
		$this->db->from('form_data');
		$this->db->where('id', $id);
		return $this->db->get()->row_array();
	}
	function get_detail($id) {
		$this->db->select();
		$this->db->from('uploaded_files');
		$this->db->where('parent_id', $id);
		return $this->db->get()->result_array();
	}
	function clear($att_id, $pdf_name) {
		$this->db->where('id', $att_id);
		$this->db->delete('form_data');
		if ($this->db->affected_rows() > 0) {
			if (file_exists(FCPATH . "uploads/docs/" . urldecode($pdf_name))) {
				unlink(FCPATH . "uploads/docs/" . urldecode($pdf_name));
			}
			return $this->remove_detail($att_id);
		}
		return false;
	}
	private function remove_detail($id) {
		$images = $this->get_detail($id);
		if (!is_array_empty($images)) {

			$upload_folder = FCPATH . "uploads/";
			foreach ($images as $key => $val) {
				$file_name = $val['file_name'];
				$full_size_path = $upload_folder . "full_size/" . $file_name;
				$pdf_path = $upload_folder . "pdf/" . $file_name;
				$thumb_path = $upload_folder . "thumbs/" . $file_name;
				if (file_exists($full_size_path)) {
					$success = unlink($full_size_path);
				}if (file_exists($pdf_path)) {
					$success = unlink($pdf_path);
				}
				if (file_exists($thumb_path)) {
					$success = unlink($thumb_path);
				}
				if (file_exists($upload_folder . $file_name)) {
					$success = unlink($upload_folder . $file_name);
				}
			}
			$this->db->where('parent_id', $id);
			$this->db->delete('uploaded_files');
			return $this->db->affected_rows();
		} else {
			return TRUE;
		}
	}
	function count_all($term = "aends") {
		$this->db->select()->from('form_data');
		if ($term != "aends") {
			$this->db->like('username', $term);
			$this->db->or_like('email', $term);
		}
		return $this->db->count_all_results();
	}
}

/* End of file Att_email_model.php */
/* Location: ./application/models/Att_email_model.php */