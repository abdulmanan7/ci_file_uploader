<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>isabellondon.com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
<div>
   <div style="font-size: 26px;font-weight: 700;letter-spacing: -0.02em;line-height: 32px;color: #41637e;font-family: sans-serif;text-align: center" align="center" id="emb-email-header"><!-- <img style="border: 0;-ms-interpolation-mode: bicubic;display: block;Margin-left: auto;Margin-right: auto;max-width: 152px" src="" alt="" width="152" height="108"> --></div>

<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">User Name : <?php echo $user['username'];?>,</p>

<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">Email : <?php echo $user['email'];?>,</p>

<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">Phone : <?php echo $user['phone'];?>,</p>

<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 25px;Margin-bottom: 25px">Comments : <?php echo $user['comments'];?></p>
<section style="width:100%">
<?php foreach ($images as $key => $image): ?>
	<a href="<?=base_url('uploads/' . $image['file_name'])?>" target="_blank">
		<img src="<?=base_url('uploads/thumbs/' . $image['file_name'])?>" alt="" style="width:30%;float:left;">
	</a>
<?php endforeach?>
</section>
</div>
</body>
</html>