  <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li><a href="#" class="heading">Tables</a></li>
            <li class="hidden-sm hidden-xs">
              <a href="#" class="selected">Pricing Plans</a>
            </li>
            <li class="hidden-sm hidden-xs">
              <a href="tables.html">Tables</a>
            </li>
          </ul>
          <div class="custom-search hidden-sm hidden-xs">
            <input type="text" class="search-query" placeholder="Search here ...">
            <i class="fa fa-search"></i>
          </div>
        </div>
        <!-- Sub Nav End -->