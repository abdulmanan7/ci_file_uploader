<!doctype html>

<!--[if lt IE 7]><html lang="en-GB" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html lang="en-GB" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html lang="en-GB" class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-GB" class="no-js"><!--<![endif]-->


<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">

    <!-- Google Chrome Frame for IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title></title>
    <!-- mobile meta (hooray!) -->
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) -->
    <link rel="apple-touch-icon" href="<?=base_url()?>assets/images/apple-icon-touch.png">
    <link rel="icon" href="wp-content/themes/meat/favicon.png">
    <!--[if IE]>
      <link rel="shortcut icon" href="http://image_uploader.co.uk/wp-content/themes/meat/favicon.ico">
    <![endif]-->
    <!-- or, set /favicon.ico for IE10 win -->
    <meta name="msapplication-TileColor" content="#f01d4f">
    <meta name="msapplication-TileImage" content="<?=base_url()?>assets/images/win8-tile-icon.png">

    <link rel="pingback" href="xmlrpc.php">

    <!-- wordpress head functions -->
    <link rel="alternate" type="application/rss+xml" title="image_uploader &raquo; Feed" href="feed/index.html" />
    <link href="<?=base_url()?>assets/fonts/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
     <link rel="stylesheet" href="<?=base_url()?>assets/plugins/upload/css/jquery.fileupload.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/plugins/upload/css/jquery.fileupload-ui.css">
        <!-- CSS adjustments for browsers with JavaScript disabled -->
        <noscript>&lt;link rel="stylesheet" href="<?=base_url()?>/assets/plugins/upload/css/jquery.fileupload-noscript.css"&gt;</noscript>
        <noscript>&lt;link rel="stylesheet" href="<?=base_url()?>/assets/plugins/upload/css/jquery.fileupload-ui-noscript.css"&gt;</noscript>
<link rel="alternate" type="application/rss+xml" title="image_uploader &raquo; Comments Feed" href="comments/feed/index.html" />
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='bones-stylesheet-css'  href='<?=base_url()?>assets/css/style.css' type='text/css' media='all' />
<!--[if lt IE 9]>
<link rel='stylesheet' id='bones-ie-only-css'  href='http://image_uploader.co.uk/<?=base_url()?>assets/css/ie.css' type='text/css' media='all' />
<![endif]-->
<script type='text/javascript' src='<?=base_url()?>assets/js/jquery.js'></script>
<script src="<?=base_url()?>/assets/plugins/upload/js/jquery.js"></script>
<script type='text/javascript' src='<?=base_url()?>assets/js/jquery-migrate.min.js'></script>
<script type='text/javascript' src='<?=base_url()?>assets/js/jquery.json-1.3.js'></script>
<script type='text/javascript' src='<?=base_url()?>assets/js/gravityforms.min.js'></script>
<script type='text/javascript' src='<?=base_url()?>assets/js/jquery.textareaCounter.plugin.js'></script>
<script type='text/javascript' src='<?=base_url()?>assets/js/placeholders.jquery.min.js'></script>
    <!-- end of wordpress head -->

	<!--[if lt IE 9]>
	   <script>
	      document.createElement('header');
	      document.createElement('nav');
	      document.createElement('section');
	      document.createElement('article');
	      document.createElement('aside');
	      document.createElement('footer');
	   </script>

		<style>header, nav, section, article, aside, footer {
	   display:block;}</style>
	<![endif]-->

  </head>

  <body class="home blog">

    <div id="container">

      <header class="header" role="banner">
        <div id="inner-header" class="wrap clearfix">

          LOGO GOES HERE...<p>

        </div> <!-- end #inner-header -->

      </header> <!-- end header -->

			<div id="content" class="gutter-top gutter-bottom">

				<div id="inner-content" class="wrap clearfix">



					<div id="upload" class="fivecol last clearfix" role="main">
						<h1 class="text-center">
							Welcome to our upload page please fill in the form below and upload your pictures by clicking "Select Photos"
						</h1>


                <div class='gf_browser_unknown gform_wrapper' id='gform_wrapper_1' ><a id='gf_1' class='gform_anchor' ></a>
              <!--   <form method='post' enctype='multipart/form-data' target='gform_ajax_frame_1' id='gform_1'  action='<?=base_url('')?>'> -->
              <form id="fileupload" action="<?=base_url()?>/upload/save" method="POST" enctype="multipart/form-data">
                        <div class='gform_body'><ul id='gform_fields_1' class='gform_fields top_label form_sublabel_below description_below'><li id='field_1_3' class='gfield gfield_contains_required field_sublabel_below field_description_below' ><label class='gfield_label' for='input_1_3' >eBay user ID<span class='gfield_required'>*</span></label><div class='ginput_container'><input name='username' id='input_1_3' type='text' value='' class='medium'  tabindex='1'  placeholder='eBay user ID' /></div><div class='gfield_description'>Please enter your eBay, Etsy or Facebook ID here.</div></li><li id='field_1_2' class='gfield gfield_contains_required field_sublabel_below field_description_below' ><label class='gfield_label' for='input_1_2' >Email Address<span class='gfield_required'>*</span></label><div class='ginput_container'>
                            <input name='email' id='input_1_2' type='email' value='' class='medium' tabindex='2'   placeholder='Email Address'/>
                        </div></li><li id='field_1_9' class='gfield gfield_contains_required field_sublabel_below field_description_below' ><label class='gfield_label'  ><span class='gfield_required'>*</span></label><div class='ginput_container'><div id='gform_multifile_upload_1_9' data-settings='{&quot;runtimes&quot;:&quot;html5,flash,html4&quot;,&quot;browse_button&quot;:&quot;gform_browse_button_1_9&quot;,&quot;container&quot;:&quot;gform_multifile_upload_1_9&quot;,&quot;drop_element&quot;:&quot;gform_drag_drop_area_1_9&quot;,&quot;filelist&quot;:&quot;gform_preview_1_9&quot;,&quot;unique_names&quot;:true,&quot;file_data_name&quot;:&quot;file&quot;,&quot;url&quot;:&quot;http:\/\/image_uploader.co.uk\/?gf_page=74e52b7283f0134&quot;,&quot;flash_swf_url&quot;:&quot;http:\/\/image_uploader.co.uk\/wp-includes\/js\/plupload\/plupload.flash.swf&quot;,&quot;silverlight_xap_url&quot;:&quot;http:\/\/image_uploader.co.uk\/wp-includes\/js\/plupload\/plupload.silverlight.xap&quot;,&quot;filters&quot;:{&quot;mime_types&quot;:[{&quot;title&quot;:&quot;Allowed Files&quot;,&quot;extensions&quot;:&quot;jpg,jpg,png,pdf,gif,tif,bmp,jpeg,jpeg&quot;}],&quot;max_file_size&quot;:&quot;50331648b&quot;},&quot;multipart&quot;:true,&quot;urlstream_upload&quot;:false,&quot;multipart_params&quot;:{&quot;form_id&quot;:1,&quot;field_id&quot;:9},&quot;gf_vars&quot;:{&quot;max_files&quot;:&quot;13&quot;,&quot;message_id&quot;:&quot;gform_multifile_messages_1_9&quot;,&quot;disallowed_extensions&quot;:[&quot;php&quot;,&quot;asp&quot;,&quot;aspx&quot;,&quot;cmd&quot;,&quot;csh&quot;,&quot;bat&quot;,&quot;html&quot;,&quot;hta&quot;,&quot;jar&quot;,&quot;exe&quot;,&quot;com&quot;,&quot;js&quot;,&quot;lnk&quot;,&quot;htaccess&quot;,&quot;phtml&quot;,&quot;ps1&quot;,&quot;ps2&quot;,&quot;php3&quot;,&quot;php4&quot;,&quot;php5&quot;,&quot;php6&quot;,&quot;py&quot;,&quot;rb&quot;,&quot;tmp&quot;]}}' class='gform_fileupload_multifile'>
										<div id='gform_drag_drop_area_1_9' class='gform_drop_area'>
											<p class='gform_drop_instructions'>Drop files here or </p>
											<div class="gform_button button fileinput-button">
      <i class="glyphicon glyphicon-plus"></i>
      <span>Select Files</span>
      <input type="file" name="userfile" multiple="">
    </div>
										</div>
									</div><span id='extensions_message' class='screen-reader-text'>Accepted file types: jpg, jpg, png, pdf, gif, tif, bmp, jpeg, jpeg.</span>
                  <div class='validation_message'>
							</div>
              <div class="dragDrop">
      <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
    </div>
              </div><div id='gform_preview_1_9'></div></li><li id='field_1_6' class='gfield field_sublabel_below field_description_below' ><label class='gfield_label' for='input_1_6' >If you have ordered more than one phone case, please use the space below to let us know the phone model and case colour for this set of images. Thank you!</label><div class='ginput_container'>
					<textarea name='comments' id='input_1_6' class='textarea medium' tabindex='4'    rows='10' cols='50'></textarea>
				</div></li><li id='field_1_7' class='gfield gform_hidden field_sublabel_below field_description_below' ></li><li id='field_1_8' class='gfield gform_hidden field_sublabel_below field_description_below' ></li><li id='field_1_10' class='gfield gform_validation_container field_sublabel_below field_description_below' ><label class='gfield_label' for='input_1_10' >Email</label><div class='ginput_container'><input name='input_10' id='input_1_10' type='text' value='' autocomplete='off'/></div><div class='gfield_description'>This field is for validation purposes and should be left unchanged.</div></li>
                            </ul></div>
        <div class='gform_footer top_label'> <input type='submit' id='gform_submit_button_1' class='gform_button button' value='Upload Photos' tabindex='5' onclick='if(window["gf_submitting_1"]){return false;}  if( !jQuery("#gform_1")[0].checkValidity || jQuery("#gform_1")[0].checkValidity()){window["gf_submitting_1"]=true;}  ' /> <input type='hidden' name='gform_ajax' value='form_id=1&amp;title=&amp;description=&amp;tabindex=1' />
            <input type='hidden' class='gform_hidden' name='is_submit_1' value='1' />
            <input type='hidden' class='gform_hidden' name='gform_submit' value='1' />

            <input type='hidden' class='gform_hidden' name='gform_unique_id' value='' />
            <input type='hidden' class='gform_hidden' name='state_1' value='WyJbXSIsImIyNzAyMjE3ZjA2NjFhZjA5ZmY2NWY4NmM1YjA3YmZlIl0=' />
            <input type='hidden' class='gform_hidden' name='gform_target_page_number_1' id='gform_target_page_number_1' value='0' />
            <input type='hidden' class='gform_hidden' name='gform_source_page_number_1' id='gform_source_page_number_1' value='1' />
            <input type='hidden' name='gform_field_values' value='' />
            <input type='hidden' name='gform_uploaded_files' id='gform_uploaded_files_1' value='' />
        </div>
                        </form>
                        </div>
            <style>
              .preview_img{
                max-width: 100px;
                height: auto;
              }
              .gform_footer{
                padding-bottom: 3em
              }
            </style>

						<script type="text/javascript">
              jQuery(document).ready(function previewImagePussy($){
                // Gravity forms thingy
                ( function ( $ ) {
                  var url = 'wp-content/uploads/gravity_forms/1-35f75aa52f9637205f17004464f130c1/tmp/index.html';
                  var image_count = 0;
                  var interval = setInterval( function() {
                    // Has it changed?
                    var thingy = $( '#gform_uploaded_files_1' );
                    // Make sure val is not empty
                    if ( !thingy.val() )
                      return;
                    // Parse the JSON data
                    var data = JSON.parse( thingy.val() );

                    // Use input_9 as a data source
                    var images = data.input_9;
                    if ( images.length == image_count )
                      return;

                    // Update the image count
                    image_count = images.length;

                    // Loop through the "preview" list
                    $( '#gform_preview_1_9' ).children().each( function() {
                      // Get the id of each
                      var id = $( this ).attr( 'id' );

                      // Loop through each image
                      for ( var x in images ) {
                        var image = images[x].temp_filename;
                        // Match the id to the filename
                        if ( image.indexOf( id ) === -1 )
                          continue;
                        // Hide the <strong> and replace it with the image
                        var img, strong = $( this ).find( 'strong' );
                        if ( strong.is( ':visible' ) ) {
                          img = $( '<img class="preview_img">' ).insertAfter( strong );
                          strong.hide();
                        }
                        else {
                          img = strong.next();
                        }
                        // Update the URL of with the image
                        img.attr( 'src', url + image );
                        break;
                      } // x in images
                    } );
                  }, 500 );
                } ( jQuery ) );
              });
						</script>

					</div> <!-- end #main -->


					<div id="main" class="sevencol first text-center clearfix" role="main">

					<!-- -->

						<section class="gutter-bottom">

							<h2>For Best Results</h2>
							<h2>Exposure</h2>

							<div class="correct round">
								<img src="<?=base_url()?>assets/images/correct-exposure.jpg" />
								<img src="<?=base_url()?>assets/images/tick.svg" class="circle" />
							</div>

							<div class="wrong round">
								<img src="<?=base_url()?>assets/images/under-exposure.jpg" />
								<img src="<?=base_url()?>assets/images/cross.svg" class="circle" />
							</div>

							<h3>Nice bright pictures print much better.</h3>
						</section>

						<!-- -->

						<section>

							<h2>Layout</h2>

							<div class="correct round">
								<img src="<?=base_url()?>assets/images/good_landscape.jpg" />
								<img src="<?=base_url()?>assets/images/tick.svg" class="circle" />
							</div>

							<div class="wrong round">
								<img src="<?=base_url()?>assets/images/bad_landscape.jpg" />
								<img src="<?=base_url()?>assets/images/cross.svg" class="circle" />
							</div>


							<h3>A good mix of portrait and landscape will fit the design better.</h3>
						</section>

						</div> <!-- end #main -->

				</div> <!-- end #inner-content -->

			</div> <!-- end #content -->

			<footer class="footer" role="contentinfo">

				<div id="inner-footer" class="wrap clearfix">

					<p class="sixcol first clearfix">
						<a rel="nofollow" target="_blank" href="https://www.facebook.com/pages/Thefunkypress/217285885081247"><img src="<?=base_url()?>assets/images/facebook.svg" alt="Facebook"/></a>
						<a rel="nofollow" target="_blank" href="https://twitter.com/image_uploader"><img src="<?=base_url()?>assets/images/twitter.svg" alt="Twitter"/></a>
						<a rel="nofollow" target="_blank" href="http://stores.ebay.co.uk/image_uploader"><img src="<?=base_url()?>assets/images/ebay.svg" alt="eBay"/></a>
					</p>

					<p class="source-org sixcol last copyright">&copy; 2016 Image Uploader by Abdulmanan.</p>

				</div> <!-- end #inner-footer -->

			</footer> <!-- end footer -->

		</div> <!-- end #container -->

<script>
(function($, w){
  // A helper function to measure heights
  // If you're using borders or outlines
  var map_height = function (index, element) {
    return $(this).outerHeight();
  };
  // Equalise heights
  var equalise = function(element, group_size) {
    // Reset element height
    element.height('');

    // Group results
    if (group_size === undefined) { group_size = 0; }

    if (group_size == 1) {
      return;
    }
    var groups = [];
    if (group_size) {
      // Not a clone, but a different object of with the same selection
      var clone = $(element);
      while (clone.length > 0) {
        groups.push($(clone.splice(0, group_size)));
      }
    }
    else {
      groups = [element];
    }

    // Measure the heights and then apply the highest measure to all
    var heights, height;
    for (var i in groups) {
      heights = groups[i].map(map_height).get();
      height = Math.max.apply(null, heights);
      groups[i].height(height);
    }
  };
  // Expose:
  w.equalise = equalise;

  // Add jQuery plugin
  $.fn.equalise = function(size) {
    equalise(this, size);
    return this;
  };

  // Determine if an url is external:
  var is_external = function () {
    var url = this;
    return (url.hostname && url.hostname.replace(/^www\./, '') !== location.hostname.replace(/^www\./, ''));
  };
  // Expose:
  w.is_external = is_external;
})(jQuery, window);

// as the page loads, call these scripts
jQuery(document).ready(function($) {

  // Menu (button) script:
  $('body').addClass('js');
  var menu_button = $('<p>').addClass('clickable mobile-only').text(window.main_nav_name || '');
  var menu_icon = '<div class="menu-icon"><div></div><div></div><div></div></div>';
  menu_button.append(menu_icon);
  $('.nav').before(menu_button);

  $('.clickable, .nav a').click(function(event) {
    if (min_width(768)) { return; }
    var sub = $(this).next();
    if (event.which === 1 && sub.length && sub[0].nodeName.toLowerCase() == 'ul') {
      if (this.nodeName.toLowerCase() == 'p' || !sub.hasClass('show')) {
        sub.toggleClass('show');
        return false;
      }
    }
  });

  // Give external links a class
  $('a').filter(is_external)
    .addClass('external')
    .attr('target', "_blank");

});
/*
Bones Scripts File
Author: Eddie Machado

This file should contain any js scripts you want to add to the site.
Instead of calling it in the header or throwing it inside wp_head()
this file will be called automatically in the footer so as not to
slow the page load.

*/

// IE8 ployfill for GetComputed Style (for Responsive Script below)
if (!window.getComputedStyle) {
  window.getComputedStyle = function(el) {
    this.el = el;
    this.getPropertyValue = function(prop) {
      var re = /(\-([a-z]){1})/g;
      if (prop === 'float') {prop = 'styleFloat';}
      if (re.test(prop)) {
        prop = prop.replace(re, function () {
          return arguments[2].toUpperCase();
        });
      }
      return el.currentStyle[prop] ? el.currentStyle[prop] : null;
    };
    return this;
  };
}

// This function swaps out all .svg files and replaces them with .png files. Not ideal if you don't support PNG aswell, but you can't get transparencies with jpgs.

function supportsSVG() {
    return !! document.createElementNS && !! document.createElementNS('http://www.w3.org/2000/svg','svg').createSVGRect;
}
if (!supportsSVG()) {
    var imgs = document.getElementsByTagName('img');
    var dotSVG = /.*\.svg$/;
    for (var i = 0; i !== imgs.length; ++i) {
        if(imgs[i].src.match(dotSVG)) {
            imgs[i].src = imgs[i].src.slice(0, -3) + 'png';
        }
    }
}

/*! A fix for the iOS orientationchange zoom bug.
 Script by @scottjehl, rebound by @wilto.
 MIT License.
*/
(function(w){
  // This fix addresses an iOS bug, so return early if the UA claims it's something else.
  if( !( /iPhone|iPad|iPod/.test( navigator.platform ) && navigator.userAgent.indexOf( "AppleWebKit" ) > -1 ) ){ return; }
  var doc = w.document;
  if( !doc.querySelector ){ return; }
  var meta = doc.querySelector( "meta[name=viewport]" ),
    initialContent = meta && meta.getAttribute( "content" ),
    disabledZoom = initialContent + ",maximum-scale=1",
    enabledZoom = initialContent + ",maximum-scale=10",
    enabled = true,
    x, y, z, aig;
  if( !meta ){ return; }
  function restoreZoom(){
    meta.setAttribute( "content", enabledZoom );
    enabled = true; }
  function disableZoom(){
    meta.setAttribute( "content", disabledZoom );
    enabled = false; }
  function checkTilt( e ){
    aig = e.accelerationIncludingGravity;
    x = Math.abs( aig.x );
    y = Math.abs( aig.y );
    z = Math.abs( aig.z );
    // If portrait orientation and in one of the danger zones
    if( !w.orientation && ( x > 7 || ( ( z > 6 && y < 8 || z < 8 && y > 6 ) && x > 5 ) ) ){
      if( enabled ){ disableZoom(); } }
    else if( !enabled ){ restoreZoom(); } }
  w.addEventListener( "orientationchange", restoreZoom, false );
  w.addEventListener( "devicemotion", checkTilt, false );
})( this );
</script>
 <!-- The template to display files available for upload -->
    <script id="template-upload" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-upload fade">
            <td>
                <span class="preview"></span>
            </td>
            <td>
                <p class="name rawName">{%=file.name%}</p>
                <strong class="error text-danger"></strong>
            </td>
            <td>
                <p class="size">Processing...</p>
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
            </td>
            <td>
                {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                </button>
                {% } %}
                {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                </button>
                {% } %}
            </td>
        </tr>
        {% } %}
    </script>
    <!-- The template to display files available for download -->
    <script id="template-download" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-download fade">
            <td>
                <span class="preview">
                    {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                    <input type="hidden" name="image[]" value="{%=file.url%}">
                    <input type="hidden" name="file_name[]" value="{%=file.name%}">
                    {% } %}
                </span>
            </td>
            <td>
                <p class="name">
                    {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                    {% } else { %}
                    <span>{%=file.name%}</span>
                    {% } %}
                </p>
                {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                {% } %}
            </td>
            <td>
                <span class="size">{%=o.formatFileSize(file.size)%}</span>
            </td>
            <td>
                {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                </button>
                {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                </button>
                {% } %}
            </td>
        </tr>
        {% } %}
    </script>
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js')?>"></script> -->
    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    <script src="<?=base_url()?>/assets/plugins/upload/js/vendor/jquery.ui.widget.js"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="<?=base_url()?>/assets/plugins/upload/js/blueimg/tmpl.min.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="<?=base_url()?>/assets/plugins/upload/js/blueimg/load-image.all.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="<?=base_url()?>/assets/plugins/upload/js/blueimg/canvas-to-blob.min.js"></script>
    <!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
    <script src="<?=base_url()?>/assets/js/bootstrap.min.js"></script>
    <!-- blueimp Gallery script -->
    <!-- <script src="https://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script> -->
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload.js"></script>
    <!-- The File Upload processing plugin -->
    <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-process.js"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-image.js"></script>
    <!-- The File Upload audio preview plugin -->
    <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-audio.js"></script>
    <!-- The File Upload video preview plugin -->
    <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-video.js"></script>
    <!-- The File Upload validation plugin -->
    <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-validate.js"></script>
    <!-- The File Upload user interface plugin -->
    <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-ui.js"></script>
    <!-- The main application script -->
    <script src="<?=base_url()?>/assets/plugins/upload/js/main.js"></script>
    <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
</body>

</html> <!-- end page. what a ride! -->