<?php if (null !== $this->session->flashdata('msg')) {
	$message = $this->session->flashdata('msg');
}
?>
<!doctype html style="background: beige;">
<!--[if lt IE 7]><html lang="en-GB" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html lang="en-GB" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html lang="en-GB" class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-GB" class="no-js"><!--<![endif]-->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
  <meta charset="utf-8">
  <!-- Google Chrome Frame for IE -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title></title>
  <!-- mobile meta (hooray!) -->
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <!-- icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) -->
  <link rel="apple-touch-icon" href="<?=base_url()?>assets/images/apple-icon-touch.png">
  <link rel="icon" href="wp-content/themes/meat/favicon.png">
  <!--[if IE]>
  <link rel="shortcut icon" href="http://image_uploader.co.uk/wp-content/themes/meat/favicon.ico">
  <![endif]-->
  <!-- or, set /favicon.ico for IE10 win -->
  <link href="<?=base_url()?>assets/fonts/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/upload/css/jquery.fileupload.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/upload/css/jquery.fileupload-ui.css">
  <!-- CSS adjustments for browsers with JavaScript disabled -->
  <noscript>&lt;link rel="stylesheet" href="<?=base_url()?>/assets/plugins/upload/css/jquery.fileupload-noscript.css"&gt;</noscript>
  <noscript>&lt;link rel="stylesheet" href="<?=base_url()?>/assets/plugins/upload/css/jquery.fileupload-ui-noscript.css"&gt;</noscript>
  <style type="text/css">
    img.wp-smiley,
    img.emoji {
      display: inline !important;
      border: none !important;
      box-shadow: none !important;
      height: 1em !important;
      width: 1em !important;
      margin: 0 .07em !important;
      vertical-align: -0.1em !important;
      background: none !important;
      padding: 0 !important;
    }
  </style>
  <link rel='stylesheet' id='bones-stylesheet-css'  href='<?=base_url()?>assets/css/style.css' type='text/css' media='all' />
  <!--[if lt IE 9]>
  <link rel='stylesheet' id='bones-ie-only-css'  href='http://image_uploader.co.uk/<?=base_url()?>assets/css/ie.css' type='text/css' media='all' />
  <![endif]-->
  <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.js"></script>
  <script type='text/javascript' src='<?=base_url()?>assets/js/placeholders.jquery.min.js'></script>
</head>
<body class="home blog">
  <div id="container">
    <div id="content" class="gutter-top gutter-bottom">
      <div id="inner-content" class="wrap clearfix">
        <div id="upload" class="fivecol last clearfix" role="main">
          <br>
          <h4 class="text-center">
            <div id="message_box">
              <?php if (!empty($message)): ?>
                <div class="row">
                  <div class="col-sm-8">
                    <?php echo $message;?>
                  </div>
                </div>
              <?php endif?>
            </div>
            Welcome to our upload page please fill in the form below and upload your pictures by clicking "Select Photos"
          </h4>
            <form id="fileupload" action="<?=base_url()?>/upload/save" method="POST" enctype="multipart/form-data">
              <div class='gform_body'><ul id='gform_fields_1' class='gform_fields top_label form_sublabel_below description_below'><li id='field_1_3' class='gfield gfield_contains_required field_sublabel_below field_description_below' ><label class='gfield_label' for='input_1_3' >eBay user ID<span class='gfield_required'>*</span></label><div class='ginput_container'><input name='username' id='input_1_3' type='text' value='' class='medium'  tabindex='1'  placeholder='eBay user ID' /></div><div class='gfield_description'>Please enter your eBay, Etsy or Facebook ID here.</div></li><li id='field_1_2' class='gfield gfield_contains_required field_sublabel_below field_description_below' ><label class='gfield_label' for='input_1_2' >Email Address<span class='gfield_required'>*</span></label><div class='ginput_container'>
              <input name='email' id='input_1_2' type='email' value='' class='medium' tabindex='2'   placeholder='Email Address'/>
            </div></li><li id='field_1_9' class='gfield gfield_contains_required field_sublabel_below field_description_below' ><label class='gfield_label'  ><span class='gfield_required'>*</span></label><div class='ginput_container'>
            <div id='gform_drag_drop_area_1_9' class='gform_drop_area'>
              <p class='gform_drop_instructions'>Drop files here or </p>
              <div class="gform_button button fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Select Files</span>
                <input type="file" name="userfile" multiple="">
              </div>
            </div>
          </div><span id='extensions_message' class='screen-reader-text'>Accepted file types: jpg, jpg, png, pdf, gif, tif, bmp, jpeg, jpeg.</span>
          <div class='validation_message'>
          </div>
          <div class="dragDrop">
            <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
          </div>
        </div><div id='gform_preview_1_9'></div></li><li id='field_1_6' class='gfield field_sublabel_below field_description_below' ><label class='gfield_label' for='input_1_6' >If you have ordered more than one phone case, please use the space below to let us know the phone model and case colour for this set of images. Thank you!</label><div class='ginput_container'>
        <textarea name='comments' id='input_1_6' class='textarea medium' tabindex='4'    rows='10' cols='50'></textarea>
      </div></li><li id='field_1_7' class='gfield gform_hidden field_sublabel_below field_description_below' ></li><li id='field_1_8' class='gfield gform_hidden field_sublabel_below field_description_below' ></li><li id='field_1_10' class='gfield gform_validation_container field_sublabel_below field_description_below' ><label class='gfield_label' for='input_1_10' >Email</label><div class='ginput_container'><input name='input_10' id='input_1_10' type='text' value='' autocomplete='off'/></div><div class='gfield_description'>This field is for validation purposes and should be left unchanged.</div></li>
    </ul></div>
    <div class='gform_footer top_label'> <input type='submit' id='gform_submit_button_1' class='gform_button button' value='Upload Photos' tabindex='5' onclick='if(window["gf_submitting_1"]){return false;}  if( !jQuery("#gform_1")[0].checkValidity || jQuery("#gform_1")[0].checkValidity()){window["gf_submitting_1"]=true;}  ' /> <input type='hidden' name='gform_ajax' value='form_id=1&amp;title=&amp;description=&amp;tabindex=1' />
    </div>
  </form>
</div>
</div> <!-- end #main -->
<div id="main" class="sevencol first text-center clearfix" role="main">
  <!-- -->
  <section class="gutter-bottom">
    <h2>For Best Results</h2>
    <h2>Exposure</h2>
    <div class="correct round">
      <img src="<?=base_url()?>assets/images/correct-exposure.jpg" />
      <img src="<?=base_url()?>assets/images/tick.svg" class="circle" />
    </div>
    <div class="wrong round">
      <img src="<?=base_url()?>assets/images/under-exposure.jpg" />
      <img src="<?=base_url()?>assets/images/cross.svg" class="circle" />
    </div>
    <h3>Nice bright pictures print much better.</h3>
  </section>
  <!-- -->
  <section>
    <h2>Layout</h2>
    <div class="correct round">
      <img src="<?=base_url()?>assets/images/good_landscape.jpg" />
      <img src="<?=base_url()?>assets/images/tick.svg" class="circle" />
    </div>
    <div class="wrong round">
      <img src="<?=base_url()?>assets/images/bad_landscape.jpg" />
      <img src="<?=base_url()?>assets/images/cross.svg" class="circle" />
    </div>
  </section>
</div> <!-- end #main -->
</div> <!-- end #inner-content -->
</div> <!-- end #content -->
</div> <!-- end #container -->
        <!-- The template to display files available for upload -->
        <script id="template-upload" type="text/x-tmpl">
          {% for (var i=0, file; file=o.files[i]; i++) { %}
          <tr class="template-upload fade">
            <td>
              <span class="preview"></span>
            </td>
            <td>
              <p class="name rawName">{%=file.name%}</p>
              <strong class="error text-danger"></strong>
            </td>
            <td>
              <p class="size">Processing...</p>
              <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
            </td>
            <td>
              {% if (!i && !o.options.autoUpload) { %}
              <button class="btn btn-primary start" disabled>
                <i class="glyphicon glyphicon-upload"></i>
              </button>
              {% } %}
              {% if (!i) { %}
              <button class="btn btn-warning cancel">
                <i class="glyphicon glyphicon-ban-circle"></i>
              </button>
              {% } %}
            </td>
          </tr>
          {% } %}
        </script>
        <!-- The template to display files available for download -->
        <script id="template-download" type="text/x-tmpl">
          {% for (var i=0, file; file=o.files[i]; i++) { %}
          <tr class="template-download fade">
            <td>
              <span class="preview">
                {% if (file.thumbnailUrl) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                <input type="hidden" name="image[]" value="{%=file.url%}">
                <input type="hidden" name="file_name[]" value="{%=file.name%}">
                <input type="hidden" name="orig_name[]" value="{%=file.orig_name%}">
                {% } %}
              </span>
            </td>
            <td>
              <p class="name">
                {% if (file.url) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                <span>{%=file.name%}</span>
                {% } %}
              </p>
              {% if (file.error) { %}
              <div><span class="label label-danger">Error</span> {%=file.error%}</div>
              {% } %}
            </td>
            <td>
              <span class="size">{%=o.formatFileSize(file.size)%}</span>
            </td>
            <td>
              {% if (file.deleteUrl) { %}
              <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                <i class="glyphicon glyphicon-trash"></i>
              </button>
              {% } else { %}
              <button class="btn btn-warning cancel">
                <i class="glyphicon glyphicon-ban-circle"></i>
              </button>
              {% } %}
            </td>
          </tr>
          {% } %}
        </script>
        <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js')?>"></script> -->
        <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/vendor/jquery.ui.widget.js"></script>
        <!-- The Templates plugin is included to render the upload/download listings -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/blueimg/tmpl.min.js"></script>
        <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/blueimg/load-image.all.min.js"></script>
        <!-- The Canvas to Blob plugin is included for image resizing functionality -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/blueimg/canvas-to-blob.min.js"></script>
        <!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
        <script src="<?=base_url()?>/assets/js/bootstrap.min.js"></script>
        <!-- blueimp Gallery script -->
        <!-- <script src="https://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script> -->
        <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.iframe-transport.js"></script>
        <!-- The basic File Upload plugin -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload.js"></script>
        <!-- The File Upload processing plugin -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-process.js"></script>
        <!-- The File Upload image preview & resize plugin -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-image.js"></script>
        <!-- The File Upload audio preview plugin -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-audio.js"></script>
        <!-- The File Upload video preview plugin -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-video.js"></script>
        <!-- The File Upload validation plugin -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-validate.js"></script>
        <!-- The File Upload user interface plugin -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-ui.js"></script>
        <!-- The main application script -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/main.js"></script>
        <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
      </body>
                          </html> <!-- end page. what a ride! -->