<!DOCTYPE html>
<html>

<!-- Mirrored from iamsrinu.com/bluemoon-admin-theme7/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 15 Sep 2015 06:54:24 GMT -->
<head>
    <title>SOLARvent</title>
    <meta charset="UTF-8" />
    <link rel="shortcut icon" href="<?php echo ASSETS . '/img/favicon.ico'?>">

    <link href="<?php echo ASSETS . "/css/bootstrap.min.css";?>" rel="stylesheet">

    <link href="<?php echo ASSETS . '/css/new.css';?>" rel="stylesheet">
    <!-- Important. For Theming change primary-color variable in main.css  -->

    <link href="<?php echo ASSETS . '/fonts/font-awesome.min.css';?>" rel="stylesheet">

    <!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
<style>
  .page-not-found .number {
    font-size: 75px;
    color: #ed6d49;
    line-height: 100%;
}
.page-not-found .phone {
    font-size: 30px;
    color: #ed6d49;
    line-height: 100%;
}
</style>
  </head>

  <body>

    <!-- Main Container start -->
    <div class="dashboard-container">

      <div class="container">

        <!-- Row Start -->
        <div class="row">
          <div class="col-lg-4 col-md-4 col-md-offset-4">

            <div class="page-not-found center-align-text">
              <h1 class="number">Fehler : 404</h1>
                <h2>
                <?php //echo $heading;?>
                Diese Seite existiert leider nicht auf diesem Server
                </h2>
              <p>
    <?php //echo $message;?>
                <br>
                Für weitere Unterstützung kontaktieren Sie uns bitte telefonisch unter:<br/><span class="phone">+ 49 (0) 53 82 / 70 42 55 0</span>
              </p>
            </div>

          </div>
        </div>
        <!-- Row End -->
      </div>
    </div>
    <!-- Main Container end -->

    <script src="<?php echo ASSETS . '/js/jquery.js'?>"></script>
    <script src="<?php echo ASSETS . '/js/bootstrap.min.js'?>"></script>

  </body>

<!-- Mirrored from iamsrinu.com/bluemoon-admin-theme7/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 15 Sep 2015 06:54:24 GMT -->
</html>