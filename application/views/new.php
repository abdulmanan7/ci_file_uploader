<?php if (null !== $this->session->flashdata('msg')) {
	$message = $this->session->flashdata('msg');
}
?>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<title>Isabel London</title>
	<!-- Google Chrome Frame for IE -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title></title>
	<!-- mobile meta (hooray!) -->
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<!-- icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) -->
	<link rel="apple-touch-icon" href="<?=base_url()?>assets/images/apple-icon-touch.png">
	<link rel="icon" href="wp-content/themes/meat/favicon.png">
  <!--[if IE]>
  <link rel="shortcut icon" href="http://image_uploader.co.uk/wp-content/themes/meat/favicon.ico">
  <![endif]-->
  <!-- or, set /favicon.ico for IE10 win -->
  <link href="<?=base_url()?>assets/fonts/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/upload/css/jquery.fileupload.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/upload/css/jquery.fileupload-ui.css">
  <!-- CSS adjustments for browsers with JavaScript disabled -->
  <noscript>&lt;link rel="stylesheet" href="<?=base_url()?>/assets/plugins/upload/css/jquery.fileupload-noscript.css"&gt;</noscript>
  <noscript>&lt;link rel="stylesheet" href="<?=base_url()?>/assets/plugins/upload/css/jquery.fileupload-ui-noscript.css"&gt;</noscript>
  <link rel='stylesheet' id='bones-stylesheet-css'  href='<?=base_url()?>assets/css/style.css' type='text/css' media='all' />
  <!--[if lt IE 9]>
  <link rel='stylesheet' id='bones-ie-only-css'  href='http://image_uploader.co.uk/<?=base_url()?>assets/css/ie.css' type='text/css' media='all' />
  <![endif]-->
  <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.js"></script>
  <script type='text/javascript' src='<?=base_url()?>assets/js/placeholders.jquery.min.js'></script>
</head>
<body>
	<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<img src="<?=base_url()?>assets/images/logo.jpg" class="logo" />
		</div>
	</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 header">
			<h1 class="text-center xh">Welcome to our Upload Page</h1>
			<h3 class="text-center">Please fill in the form below and upload your pictures by clicking "Select Photos"</h3>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<h3 class="">For Best Results</h3>
				<p class="">Please send the Good size Quality photos</p>
				<img src="<?=base_url()?>assets/images/p1.png" class="example-img" />
				<img src="<?=base_url()?>assets/images/p2.png" class="example-img" />
				<br>

				<img src="<?=base_url()?>assets/images/question.png" class="q-img" />
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<div id="message_box">
				<?php if (!empty($message)): ?>
					<div class="row">
						<div class="col-xs-12">
							<?php echo $message;?>
						</div>
					</div>
				<?php endif?>
			</div>
				<form id="fileupload" action="<?=base_url()?>/upload/save" method="POST" enctype="multipart/form-data">
					<div class="form-group">
						<label for="">Amazon order number *</label>
						<input type="text" class="form-control" name="username" required="required">
					</div>
					<div class="form-group">
						<label for="">Email Address *</label>
						<input type="text" class="form-control" id="" placeholder="Enter correct Email for send the proof" name="email" required="required">
					</div>
					<div class="form-group">
						<label for="">Mobile Number</label>
						<input type="text" class="form-control" name="phone">
					</div>
					<div class='ginput_container'>
						<div id='gform_drag_drop_area_1_9' class='gform_drop_area'>
							<p class='gform_drop_instructions'>Drop files here or </p>
							<div class="gform_button button fileinput-button">
								<i class="glyphicon glyphicon-plus"></i>
								<span>Select Files</span>
								<input type="file" name="userfile" multiple="">
							</div>
						</div>
            <span>Accepted file types: Jpg, Jpeg, Png, Pdf, Gif, Tif, Bmp.</span>
					</div>
					<div class="dragDrop">
            <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
          </div>
					<br>
          <label for="comments">Your Message *</label>
					<textarea name="comments" class="form-control" rows="3" required="required"></textarea>
					<p class="help-block">if you have ordered more then one item,<br>please send in separate upload and don't mix Everything in one upload.</p>
					<br>
					<button type="submit" class="btn btn-info" style="padding: 18px 54px;">UPLOAD</button>
					<br>
					<p class="help-block">* Mandatory Fields</p>
				</form>
			</div>
			<br>
		</div>
		<div class="row">
			<div class="footer">
				<p class="text-center bottom-text">
          We respect your privacy and take protect it seriously and we don't share your personal information with anyone<br> and we don't use for the promotional or advertising purposes. Please send correct information for deliver excellent service.<br>
					Thanks
				</p>
        <p href="#" class="text-center">© 2016 Isabel London</p>
			</div>
		</div>
	</div>
 <script id="template-upload" type="text/x-tmpl">
          {% for (var i=0, file; file=o.files[i]; i++) { %}
          <tr class="template-upload fade">
            <td>
              <span class="preview"></span>
            </td>
            <td>
              <p class="name rawName">{%=file.name%}</p>
              <strong class="error text-danger"></strong>
            </td>
            <td>
              <p class="size">Processing...</p>
              <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
            </td>
            <td>
              {% if (!i && !o.options.autoUpload) { %}
              <button class="btn btn-primary start" disabled>
                <i class="glyphicon glyphicon-upload"></i>
              </button>
              {% } %}
              {% if (!i) { %}
              <button class="btn btn-warning cancel">
                <i class="glyphicon glyphicon-ban-circle"></i>
              </button>
              {% } %}
            </td>
          </tr>
          {% } %}
        </script>
        <!-- The template to display files available for download -->
        <script id="template-download" type="text/x-tmpl">
          {% for (var i=0, file; file=o.files[i]; i++) { %}
          <tr class="template-download fade">
            <td>
              <span class="preview">
                {% if (file.thumbnailUrl) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                <input type="hidden" name="image[]" value="{%=file.url%}">
                <input type="hidden" name="file_name[]" value="{%=file.name%}">
                <input type="hidden" name="orig_name[]" value="{%=file.orig_name%}">
                {% } %}
              </span>
            </td>
            <td>
              <p class="name">
                {% if (file.url) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                <span>{%=file.name%}</span>
                {% } %}
              </p>
              {% if (file.error) { %}
              <div><span class="label label-danger">Error</span> {%=file.error%}</div>
              {% } %}
            </td>
            <td>
              <span class="size">{%=o.formatFileSize(file.size)%}</span>
            </td>
            <td>
              {% if (file.deleteUrl) { %}
              <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                <i class="glyphicon glyphicon-trash"></i>
              </button>
              {% } else { %}
              <button class="btn btn-warning cancel">
                <i class="glyphicon glyphicon-ban-circle"></i>
              </button>
              {% } %}
            </td>
          </tr>
          {% } %}
        </script>
        <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js')?>"></script> -->
        <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/vendor/jquery.ui.widget.js"></script>
        <!-- The Templates plugin is included to render the upload/download listings -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/blueimg/tmpl.min.js"></script>
        <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/blueimg/load-image.all.min.js"></script>
        <!-- The Canvas to Blob plugin is included for image resizing functionality -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/blueimg/canvas-to-blob.min.js"></script>
        <!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
        <script src="<?=base_url()?>/assets/js/bootstrap.min.js"></script>
        <!-- blueimp Gallery script -->
        <!-- <script src="https://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script> -->
        <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.iframe-transport.js"></script>
        <!-- The basic File Upload plugin -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload.js"></script>
        <!-- The File Upload processing plugin -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-process.js"></script>
        <!-- The File Upload image preview & resize plugin -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-image.js"></script>
        <!-- The File Upload audio preview plugin -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-audio.js"></script>
        <!-- The File Upload video preview plugin -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-video.js"></script>
        <!-- The File Upload validation plugin -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-validate.js"></script>
        <!-- The File Upload user interface plugin -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/jquery.fileupload-ui.js"></script>
        <!-- The main application script -->
        <script src="<?=base_url()?>/assets/plugins/upload/js/main.js"></script>
        <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
      </body>
                          </html> <!-- end page. what a ride! -->